// 判断是不是6-18位数字字母下划线组合
function isTrueName(s){
    var patrn=/^(\w){6,20}$/;
    if (!patrn.exec(s)){
        return true
    }else{
        return false
    }
}
// 根据时间获取当前星期
function getDayNow(dayValue){   
    var day = new Date(Date.parse(dayValue.replace(/-/g, '/')));                              
    var today = new Array("周日","周一","周二","周三","周四","周五","周六");   
    return today[day.getDay()];      
}
// 判断金额的正则
function panMoney(this_,text){
	var money=this_.val();
	var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
	if (reg.test(money)) {
          
    }else{
        alertPublic(text);
        this_.val("");
        return true
    };
}
// 判断是否为正整数
function integer(this_,text){
	var integer=this_.val();
  	var re = /^[0-9]*[1-9][0-9]*$/ ;  
  	if (re.test(integer)) {
        
   	}else{
        alertPublic(text);
        this_.val("");
   	};
}
// 根据指定id删除数组对象
function removeItem(arrObj,key){
  	for(var i=0;i<arrObj.length;i++){
	    if(arrObj[i].drugId == key){
	      	arrObj.splice(i, 1)
	    }
  	}
  	return arrObj
}
// 数组对象去重
function arrRepeat(arr){
  	var result = [];
  	var obj = {};
  	for(var i =0; i<arr.length; i++){
     	if(!obj[arr[i].drugId]){
        	result.push(arr[i]);
        	obj[arr[i].drugId] = true;
     	}
  	}
  	return result
}