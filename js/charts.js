
var truth =[18203, 23489, 29034, 194970]
var practice=[19325, 23438, 31000, 91594]
function createchart(){
        option = {
    title : {
        text: '综合成绩评定',
        // subtext: '数据来自系统内部'
    },
    tooltip : {
        trigger: 'axis'
    },
    legend: {
        data:['理论', '实际']
    },
    toolbox: {
        show : true,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType: {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'value',
            boundaryGap : [0, 0.01]
        }
    ],
    yAxis : [
        {
            type : 'category',
            data : ['社会服务能力','学工能力','科研能力','教学能力']
        }
    ],
    series : [
        {
            name:'理论',
            type:'bar',
            data:[truth[0], truth[1], truth[2], truth[3]]
        },
        {
            name:'实际',
            type:'bar',
            data:[practice[0],practice[1],practice[2],practice[3]]
        }
    ]
};
        //初始化echarts实例
        var myChart = echarts.init(document.getElementById('chartmain'));

        //使用制定的配置项和数据显示图表
        myChart.setOption(option);
}
createchart();

function add1(){
    // console.log($(this).text());

    truth[0] += 1000;
    console.log(truth[0]);
    createchart();
}
function less1(){
    truth[0] -= 1000;
    console.log(truth[0]);
    createchart();
}
function add2(){
    // console.log($(this).text());

    truth[1] += 1000;
    // console.log(truth[0]);
    createchart();
}
function less2(){
    truth[1] -= 1000;
    // console.log(truth[1]);
    createchart();
}
function add3(){
    // console.log($(this).text());

    truth[2] += 1000;
    // console.log(truth[0]);
    createchart();
}
function less3(){
    truth[2] -= 1000;
    // console.log(truth[1]);
    createchart();
}
function add4(){
    // console.log($(this).text());

    truth[3] += 1000;
    // console.log(truth[0]);
    createchart();
}
function less4(){
    truth[3] -= 1000;
    // console.log(truth[1]);
    createchart();
}

function add11(){
    // console.log($(this).text());

    practice[0] += 1000;
    console.log(practice[0]);
    createchart();
}
function less11(){
    practice[0] -= 1000;
    console.log(practice[0]);
    createchart();
}
function add22(){
    // console.log($(this).text());

    practice[1] += 1000;
    // console.log(truth[0]);
    createchart();
}
function less22(){
    practice[1] -= 1000;
    // console.log(truth[1]);
    createchart();
}
function add33(){
    // console.log($(this).text());

    practice[2] += 1000;
    // console.log(truth[0]);
    createchart();
}
function less33(){
     practice[2] -= 1000;
    // console.log(truth[1]);
    createchart();
}
function add44(){
    // console.log($(this).text());

     practice[3] += 1000;
    // console.log(truth[0]);
    createchart();
}
function less44(){
     practice[3] -= 1000;
    // console.log(truth[1]);
    createchart();
}
    